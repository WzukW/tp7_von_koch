#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TP CLément Joly, MPSI #
# NOTE: Un certain nombre de tests des fonctions ont été commentés #

### Question 1 ###

### Question 2 ###
from turtle import *
from math import cos, sin, pi
import random as r # Pour la génération des couleurs du tracé

def rotation(M,C,theta):
    """retourne l'image M2 de M par la rotation de centre C et d'angle theta"""
    # C(a, b)
    # M(x, y)
    # z affixe CM (vecteur)
    # z' affixe CM' (vecteur)
    (x, y) = M
    (a, b) = C
    x2 = a + (x - a) * cos(theta) - (y - b) * sin(theta)
    y2 = b + (x - a) * sin(theta) + (y - b) * cos(theta)
    M2=(x2,y2)
    return M2

def traceLignePolygonale(ligne):
    """trace la ligne polygonale reçue en argument"""
    clear() # efface le dessin
    up() # leve le crayon
    for i in range(len(ligne)):
        goto(ligne[i][0],ligne[i][1]) # déplace la tortue au point suivant
        if i==0:
            down() # descend le crayon au premier point

def initialisation():
    """Fonction d'initialisation traçant le triangle de départ"""
    # on choisit la taille en fonction de la hauteur de la fenêtre
    taille=2*int(0.4*window_height())
    # on crée les 3 points initiaux
    x0=taille/2
    y0=-int(x0*0.6)
    x1=x0-taille
    y1=y0
    x2,y2=rotation((x0,y0),(x1,y1),pi/3)
    ligne=[(x0,y0),(x1,y1),(x2,y2),(x0,y0)]
    return ligne

### Question 3 ###
def transformeSegment(A, E):
    """Applique la transformation de Koch entre A et B (retourne les points)"""
    (xA, yA) = A
    (xE, yE) = E
    # Différence entre les points 3 points (distance entre les deux points
    # divisée par 3)
    B = (((2*xA + xE)/3), ((2*yA + yE)/3))
    D = (((xA + 2*xE)/3), ((yA + 2*yE)/3))
    C = rotation(D, B, pi/3)
    return [ A, B, C, D, E ]

# XXX Test
# traceLignePolygonale(transformeSegment((x1,y1), (x2, y2)))

def transformeLigne(ligne):
    """Transformation d'une ligne en ligne brisée"""
    # XXX Test
    # print("ligne", len(ligne))

    ligneTransforme = [] # Ligne transformée, insertion
    # Conservation du premier et du dernier point
    for i in range(0, len(ligne) - 1): # Utilisation de 0 pour spécifier un pas
        # XXX Test
        # print("i", i)

        # Récupération de deux points successifs
        A = ligne[i]
        B = ligne[i + 1]

        # XXX Test
        # traceLignePolygonale(transformeSegment(A, B))

        ligneTransforme.extend(transformeSegment(A, B))
    return ligneTransforme

# Test
# traceLignePolygonale(transformeSegment((x1,y1), (x2, y2)))
# traceLignePolygonale(transformeLigne(transformeSegment((x1,y1), (x2, y2))))


def getColor():
    """Générer aléatoirement une couleur"""
    return (r.random(), r.random(), r.random())

# Tracer le flocon de Koch tant que l'utilisateur ne force pas l'arrêt du
# programme
figure = initialisation()
traceLignePolygonale(figure)
while True:
    figure = transformeLigne(figure)
    color(getColor())
    traceLignePolygonale(figure)

