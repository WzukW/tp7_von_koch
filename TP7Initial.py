from turtle import *
from math import cos, sin, pi

def rotation(M,C,theta):
    """retourne l'image de M par la rotation de centre C et d'angle theta"""
    return M

def traceLignePolygonale(ligne):
    """trace la ligne polygonale reçue en argument"""
    clear() # efface le dessin
    up() # leve le crayon
    for i in range(len(ligne)):
        goto(ligne[i][0],ligne[i][1]) # déplace la tortue au point suivant
        if i==0:
            down() # descend le crayon au premier point  
        
# on choisit la taille en fonction de la hauteur de la fenêtre
taille=2*int(0.4*window_height())
# on crée les 3 points initiaux
x0=taille/2
y0=-int(x0*0.6)
x1=x0-taille
y1=y0
x2,y2=rotation((x0,y0),(x1,y1),pi/3)
ligne=[(x0,y0),(x1,y1),(x2,y2),(x0,y0)]
traceLignePolygonale(ligne)
#traceLignePolygonale(transformeSegment((x1,y1),(x2,y2)))
